#include <stdio.h>
int main()
{
	int a,b,c,d,e,f;
	a = 10;
	b = 20;
	c = a + b;
	printf("The sum of two numbers = %d \t", c);
	d = b - a;
	printf("The difference between the two numbers = %d \t", d);
	e = a * b;
	printf("The product of two numbers = %d \t", e);
	f = b / a;
	printf("The division of two numbers = %d \t", f);
	return 0;
}